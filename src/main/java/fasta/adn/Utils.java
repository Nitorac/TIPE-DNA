package fasta.adn;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Utils {

    public static final String fullSeparator = "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-";
    public static final String titleDisplay = fullSeparator + "\n\r\n                           /$$$$$$$  /$$   /$$  /$$$$$$         /$$$$$$   /$$$$$$  /$$      /$$ /$$$$$$$  /$$$$$$$  /$$$$$$$$  /$$$$$$   /$$$$$$   /$$$$$$  /$$$$$$$                        \r\n                /$$/$$    | $$__  $$| $$$ | $$ /$$__  $$       /$$__  $$ /$$__  $$| $$$    /$$$| $$__  $$| $$__  $$| $$_____/ /$$__  $$ /$$__  $$ /$$__  $$| $$__  $$     /$$/$$               \r\n         /$$$$ |  $$$/    | $$  \\ $$| $$$$| $$| $$  \\ $$      | $$  \\__/| $$  \\ $$| $$$$  /$$$$| $$  \\ $$| $$  \\ $$| $$      | $$  \\__/| $$  \\__/| $$  \\ $$| $$  \\ $$    |  $$$/  /$$$$        \r\n /$$$$$$|____/ /$$$$$$$   | $$  | $$| $$ $$ $$| $$$$$$$$      | $$      | $$  | $$| $$ $$/$$ $$| $$$$$$$/| $$$$$$$/| $$$$$   |  $$$$$$ |  $$$$$$ | $$  | $$| $$$$$$$/    /$$$$$$$|____/ /$$$$$$\r\n|______/ /$$$$|__ $$$_/   | $$  | $$| $$  $$$$| $$__  $$      | $$      | $$  | $$| $$  $$$| $$| $$____/ | $$__  $$| $$__/    \\____  $$ \\____  $$| $$  | $$| $$__  $$   |__ $$$_/ /$$$$|______/\r\n        |____/  /$$ $$    | $$  | $$| $$\\  $$$| $$  | $$      | $$    $$| $$  | $$| $$\\  $ | $$| $$      | $$  \\ $$| $$       /$$  \\ $$ /$$  \\ $$| $$  | $$| $$  \\ $$     /$$ $$ |____/        \r\n               |__/__/    | $$$$$$$/| $$ \\  $$| $$  | $$      |  $$$$$$/|  $$$$$$/| $$ \\/  | $$| $$      | $$  | $$| $$$$$$$$|  $$$$$$/|  $$$$$$/|  $$$$$$/| $$  | $$    |__/__/               \r\n                          |_______/ |__/  \\__/|__/  |__/       \\______/  \\______/ |__/     |__/|__/      |__/  |__/|________/ \\______/  \\______/  \\______/ |__/  |__/                       \r\n\r\n" + fullSeparator;

    public static String ask(String message, boolean hasToTrim, boolean areConditionsStricts, String... loopConditions){
        String resp = "";
        loopConditions = (loopConditions.length == 0) ? new String[]{""} : loopConditions;
        boolean hasToStop = false;
        while(!hasToStop){
            resp = readLine(message);
            hasToStop = false;
            for(String e : loopConditions){
                if(!areConditionsStricts ^ (hasToTrim ? resp.trim() : resp).equalsIgnoreCase((hasToTrim ? e.trim() : e))){
                    hasToStop = true;
                }
            }
        }
        return resp;
    }

    public static String ask(String message, boolean areConditionsStricts, String... loopConds){
        return ask(message, true, areConditionsStricts, loopConds);
    }

    public static String ask(String message, String... loopConds){
        return ask(message, true, loopConds);
    }

    public static String readLine(String format, Object... args){
        if (System.console() != null) {
            return System.console().readLine(format, args);
        }
        System.out.print(String.format(format, args));
        BufferedReader reader = new BufferedReader(new InputStreamReader(
            System.in));
        try {
            return reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
