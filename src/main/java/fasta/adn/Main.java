package fasta.adn;

import joptsimple.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.stream.Stream;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;

public class Main {

    public static final OptionParser optsParser = new OptionParser();
    public static final ArgumentAcceptingOptionSpec<File> inputOpt = optsParser.acceptsAll(Arrays.asList("i", "input"), "Entrée : Fichier FAST du génôme (obligatoire)").withRequiredArg().ofType(File.class);
    public static final ArgumentAcceptingOptionSpec<File> outputOpt = optsParser.acceptsAll(Arrays.asList("o", "output"), "Sortie : Dossier de destination (contenant les découpes et les compressions) (obligatoire)").withRequiredArg().ofType(File.class);
    public static final ArgumentAcceptingOptionSpec<String> compOpt = optsParser.acceptsAll(Arrays.asList("c", "comp", "compresser"), "Compresser : Compresse avec l'algo choisi l'ADN en fichier binaire").withRequiredArg();
    public static final ArgumentAcceptingOptionSpec<String> decompOpt = optsParser.acceptsAll(Arrays.asList("d", "decomp", "decompresser"), "Décompresser : Décompresse avec l'algo choisi le fichier binaire en ADN").withRequiredArg();
    public static final ArgumentAcceptingOptionSpec<String> splitOpt = optsParser.acceptsAll(Arrays.asList("s", "split", "decouper"), "Découper : Découpe un fichier FAST en plusieurs FAST (1 fichier / génome)").withOptionalArg();
    public static final ArgumentAcceptingOptionSpec<String> debugOpt = optsParser.acceptsAll(Arrays.asList("d", "debug"), "Debug : Affiche les messages de debug").withOptionalArg();
    public static OptionSet optSet;
    public static OptionSpec chosenOpt;

    public static boolean debug = false;

    public static File input;
    public static File output;

    public static void main(String[] args) throws IOException, IllegalAccessException {
        optsParser.allowsUnrecognizedOptions();
        optsParser.accepts("help", "Affiche l'aide").forHelp();
        log("");
        log(Utils.titleDisplay);
        log("");
        if((optSet = validateCommand(args)) == null){
            return;
        }

        debug = optSet.has(debugOpt);

        if(chosenOpt.equals(decompOpt)){

        }else if(chosenOpt.equals(compOpt)){

        }else if(chosenOpt.equals(splitOpt)){
            splitFiles();
        }
    }

    public static void startUI(OptionSet optSet){
        log("");
        log("");
        if(!optSet.has(inputOpt)){
            /*String answer = "";
            while(!answer.equals("1") || !answer.equals("2") || !answer.equals("3")){
                answer = console.readLine("Votre réponse : ");
            }*/
            String baseString;
            input = new File(((baseString = Utils.ask("Entrez l'arborescence du fichier source : ", false)).startsWith("\"") && baseString.endsWith("\"")) ? baseString.substring(1, baseString.length() - 1) : baseString);
            log("");
        }else{
            input = optSet.valueOf(inputOpt);
        }

        if(!optSet.has(outputOpt)){
            /*String answer = "";
            while(!answer.equals("1") || !answer.equals("2") || !answer.equals("3")){
                answer = console.readLine("Votre réponse : ");
            }*/
            String baseString;
            output = new File(((baseString = Utils.ask("Entrez l'arborescence du dossier de destination : ", false)).startsWith("\"") && baseString.endsWith("\"")) ? baseString.substring(1, baseString.length() - 1) : baseString);
        }else{
            output = optSet.valueOf(outputOpt);
        }

        log("");
        log("");
        log("Vous n'avez pas entrez d'option, veuillez choisir celle que vous souhaitez :");
        log("");
        log("    1. " + splitOpt.description());
        log("    2. " + compOpt.description());
        log("    3. " + decompOpt.description());
        log("");

        /*String answer = "";
        while(!answer.equals("1") || !answer.equals("2") || !answer.equals("3")){
            answer = console.readLine("Votre réponse : ");
        }*/

        String answer = Utils.ask("Votre choix : ", "1", "2", "3");
        log("");

        switch(Integer.valueOf(answer)){
            case 1:
                chosenOpt = splitOpt;
                break;
            case 2:
                chosenOpt = compOpt;
                break;
            case 3:
                chosenOpt = decompOpt;
                break;
        }
    }

    public static OptionSet validateCommand(String[] args) throws IOException, IllegalAccessException {
        OptionSet optSet;
        try {
            optSet = optsParser.parse(args);
        }
        catch(OptionException e) {
            optsParser.printHelpOn(System.out);
            return null;
        }

        if((optSet.has(inputOpt) && optSet.has(outputOpt)) && (optSet.has(compOpt) || optSet.has(decompOpt) || optSet.has(splitOpt))){
            chosenOpt = optSet.specs().stream().filter(optSpec -> !optSpec.equals(inputOpt) && !optSpec.equals(outputOpt)).findFirst().orElseThrow(() -> new IllegalArgumentException("L'option aurait du être gérée avant ..."));
            input = optSet.valueOf(inputOpt);
            output = optSet.valueOf(outputOpt);
        }else{
            startUI(optSet);
        }

        File o;
        if(!input.exists()){
            log(input.getAbsolutePath());
            throw new FileNotFoundException("Le fichier source est introuvable");
        }else if(!(o = output).exists()){
            /*String resp = "";
            while(!resp.toLowerCase().equals("o") && !resp.toLowerCase().equals("n")){
                resp = console.readLine("Le dossier de sortie (" + o + ") n'existe pas, voulez-vous tenter de le créer ? O/N ");
            }*/
            String resp = Utils.ask("Le dossier de sortie (" + o + ") n'existe pas, voulez-vous tenter de le créer (O/N) ? ", "o", "n");

            if(resp.toLowerCase().equals("o")){
                if(!o.mkdirs()){
                    throw new IllegalAccessException("Impossible de créer les dossiers");
                }
            }
        }
        return optSet;
    }

    public static void splitFiles() throws IOException {
        log("");
        log("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
        log("|  ---->    Découpage de fichier enclenché !    <----  |");
        log("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
        log("");
        log("Veuillez patienter ...");
        log("");
        try (Stream<String> stream = Files.lines(Paths.get(input.toURI()))) {

            final File[] currentFile = new File[1];
            currentFile[0] = null;
            ArrayList<String> lines = new ArrayList<>();
            String[] currentParam = new String[1];

            stream.forEach(line -> {
                if(line.startsWith(">")){
                    if(currentParam[0] != null){
                        try {
                            currentFile[0] = new File(output, currentParam[0]);
                            Files.write(currentFile[0].toPath(), lines, UTF_8, TRUNCATE_EXISTING, CREATE);
                            log("Nouveau fichier crée : " + currentParam[0] + " ( " + String.format(Locale.FRANCE, "%,d", lines.size()) + " ligne(s) )");
                            currentParam[0] = line.substring(1);
                            lines.clear();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }else{
                        currentParam[0] = line.substring(1);
                    }
                }else if(currentParam[0] != null){
                    String adaptedLine;
                    if(!(adaptedLine = line.replaceAll("N", "").trim()).isEmpty()){
                        lines.add(adaptedLine);
                    }
                }
            });
        }

        log("");
        log("Découpage terminé avec succès dans " + output + " !");
    }

    public static void debug(String msg){
        if(debug){
            log("[Debug] " + msg);
        }
    }

    public static void log(String msg){
        System.out.println(msg);
    }
}
